# gatk4 Singularity container
### Bionformatics package gatk4<br>
The GATK is the industry standard for identifying SNPs and indels in germline DNA and RNAseq data. Its scope is now expanding to include somatic short variant calling, and to tackle copy number (CNV) and structural variation (SV). In addition to the variant callers themselves, the GATK also includes many utilities to perform related tasks such as processing and quality control of high-throughput sequencing data, and bundles the popular Picard toolkit. https://www.broadinstitute.org/gatk/<br>
gatk4 Version: 4.1.7.0<br>
[https://github.com/broadinstitute/gatk]

Singularity container based on the recipe: Singularity.gatk4_v4.1.7.0

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build gatk4_v4.1.7.0.sif Singularity.gatk4_v4.1.7.0`

### Get image help
`singularity run-help ./gatk4_v4.1.7.0.sif`

#### Default runscript: STAR
#### Usage:
  `gatk4_v4.1.7.0.sif --help`<br>
    or:<br>
  `singularity exec gatk4_v4.1.7.0.sif gatk --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull gatk4_v4.1.7.0.sif oras://registry.forgemia.inra.fr/gafl/singularity/gatk4/gatk4:latest`


